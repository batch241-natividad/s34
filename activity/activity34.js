
const express = require("express");


const app = express();

const port = 3000;

let users = [
  {"username": 'Johndoe',
  "password": 'johndoe123' }
];

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.get("/home", (request, response) => {
	
	response.send("Welcome to the homepage!");

});

app.get("/users", (request, response) => {

	response.json(users);

});

app.delete('/delete-user', (request, response) => {


  response.send(`User ${request.body.username} has been deleted`)
});

app.listen(port, () => console.log(`Server running at port ${port}`));